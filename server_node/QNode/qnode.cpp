
#include "../QNode/qnode.hpp"

#include <ros/network.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <string>

#include <tf/tf.h>
//#include <tf/LinearMath/Transform.h>

#include <sstream>

#include <QDebug>

QNode::QNode(int argc, char** argv) : init_argc(argc), init_argv(argv)
{
  init();
}

QNode::~QNode()
{
  if (ros::isStarted())
  {
    ros::shutdown(); // explicitly needed since we use ros::start();
    ros::waitForShutdown();
  }
  wait();
}

bool QNode::init()
{
  ros::init(init_argc, init_argv, "server_node");
  if (!ros::master::check())
  {
    return false;
  }
  ros::start(); // explicitly needed since our nodehandle is going out of scope.
  ros::NodeHandle n;

  pub_port = n.advertise<sis_msgs::state_Server>("/Display/AGV/PORT", 1, this);

  pub_state = n.advertise<sis_msgs::state_AGV>("/Display/AGV/STATE", 1, this);

  sub_CMD = n.subscribe("/Display/ACS/CMD", 1, &QNode::cmdCallback, this);

  start();
  return true;
}

void QNode::run()
{
  ros::Rate loop_rate(5);

  while (ros::ok())
  {
      if(agv1start_)
      {
      cnt1_++;
      if(cnt1_>=30)
      {
          cnt1_ = 30;
          port_msgs.AGV1_STATE = false;
          port_msgs.AGV1_CMD = false;
          agv1start_ = false;
      }
      }
      if(agv2start_)
      {
      cnt2_++;
      if(cnt2_>=30)
      {
          cnt2_ = 30;
          port_msgs.AGV2_STATE = false;
          port_msgs.AGV2_CMD = false;
          agv2start_ = false;
      }
      }

      pub_port.publish(port_msgs);

    ros::spinOnce();
    loop_rate.sleep();
  }
  Q_EMIT
  rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)

  ros::shutdown();
}

void QNode::cmdCallback(const sis_msgs::cmd_AGV::ConstPtr& msg)
{
  uint8_t agv_id = msg->id;

  uint8_t task_cmd = msg->cmd;
  int8_t goal = msg->goal;
  int8_t sub_goal = msg->subgoal;

  int16_t dist_x = msg->x;
  int16_t dist_y = msg->y;
  int32_t dist_th = msg->th;

  int8_t conv_cmd = msg->conv;

  emit send_CMD(agv_id, task_cmd, goal, sub_goal, dist_x, dist_y, dist_th,
                conv_cmd);
}

void QNode::send_State(STATE state_)
{
  sis_msgs::state_AGV state_msgs;
  state_msgs.ID = state_.id;
  state_msgs.X = state_.X;
  state_msgs.Y = state_.Y;
  state_msgs.A = state_.A;

  state_msgs.main_vol = state_.main_vol;

  state_msgs.agv_pos = state_.agv_pos;
  state_msgs.agv_status = state_.agv_status;

  state_msgs.conv_status = state_.conv_status;

  state_msgs.emc = state_.emc;

  state_msgs.conv_in = state_.conv_in;
  state_msgs.conv_mid = state_.conv_mid;
  state_msgs.conv_arr = state_.conv_arr;

  state_msgs.stp1_up = state_.stp1_up;
  state_msgs.stp1_down = state_.stp1_down;
  state_msgs.stp2_up = state_.stp2_up;
  state_msgs.stp2_down = state_.stp2_down;

  pub_state.publish(state_msgs);

  if(state_.id == 1)
  {
      cnt1_ = 0;
      port_msgs.AGV1_CMD = true;
      port_msgs.AGV1_STATE = true;
      agv1start_ = true;
  }

  if(state_.id == 2)
  {
      cnt2_ = 0;
      port_msgs.AGV2_CMD = true;
      port_msgs.AGV2_STATE = true;
      agv2start_ = true;
  }
}

void QNode::client_disconnect(int id, int port)
{

}

void QNode::client_connect(int id, int port)
{

}

void QNode::log(const LogLevel& level, const std::string& msg)
{
  logging_model.insertRows(logging_model.rowCount(), 1);
  std::stringstream logging_model_msg;
  switch (level)
  {
  case (Debug):
  {
    ROS_DEBUG_STREAM(msg);
    logging_model_msg << "[DEBUG] [" << ros::Time::now() << "]: " << msg;
    break;
  }
  case (Info):
  {
    ROS_INFO_STREAM(msg);
    logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
    break;
  }
  case (Warn):
  {
    ROS_WARN_STREAM(msg);
    logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
    break;
  }
  case (Error):
  {
    ROS_ERROR_STREAM(msg);
    logging_model_msg << "[ERROR] [" << ros::Time::now() << "]: " << msg;
    break;
  }
  case (Fatal):
  {
    ROS_FATAL_STREAM(msg);
    logging_model_msg << "[FATAL] [" << ros::Time::now() << "]: " << msg;
    break;
  }
  }
  QVariant new_row(QString(logging_model_msg.str().c_str()));
  logging_model.setData(logging_model.index(logging_model.rowCount() - 1),
                        new_row);
  Q_EMIT loggingUpdated(); // used to readjust the scrollbar
}
