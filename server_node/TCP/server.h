#ifndef SERVER_H
#define SERVER_H
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QByteArray>
#include <QNetworkInterface>
#include <QSet>
#include <QObject>
class Server : public QTcpServer
{
    Q_OBJECT
private Q_SLOTS:
    void addClient();
    void readReceivedData();
    void displayError(QAbstractSocket::SocketError error);
    void connectionLost();
    void disconnected();
    void close_();

Q_SIGNALS:
    void readCount(QString count);
    void checkConnect(bool isConnect);

	void client_disconnect(int, int);
	void client_connect(int, int);
    
    void readInfo(int, QByteArray);
    
protected:
//    void incomingConnection(int socketfd);

public:
    explicit Server(quint16 port = 0, QObject * parent = 0);
    ~Server();
    QHostAddress getHostAddress() const;
    quint16 getPort() const;

    QString get_X();
    QString get_Y();
    QString get_Z();
    QString get_ID();

    void writeData(QByteArray str);
    void writeData(int id, QByteArray str);
    
    void printSensor(QString value);

private:

    int id;
    QTcpSocket *client;
    QSet<QTcpSocket*> clients;
    QTcpSocket *client_agv[3];
};

#endif // SERVER_H
