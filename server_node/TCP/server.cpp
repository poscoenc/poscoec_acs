#include "server.h"
#include <QDebug>
#include <QStringList>
#include <QDataStream>

#include <agv_protocol/convertToByte.hpp>


static inline QByteArray IntToArray(qint32 source);;
Server::Server(quint16 port, QObject *parent)
    : QTcpServer(parent)
    , id(0)
{

    if (!this->listen(QHostAddress::Any, port))
    {
        return;
    }
    else
    {
        qDebug()<<"Server"<<port<<"is ON";
        connect(this, SIGNAL(newConnection()), this, SLOT(addClient()));

        for(int i=0; i<3; i++)
            client_agv[i] = nullptr;
    }
}

Server::~Server()
{
    deleteLater();
}

QHostAddress Server::getHostAddress() const
{
    QHostAddress hostAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();

    for (int i = 0; i < ipAddressesList.size(); ++i)
    {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost && ipAddressesList.at(i).toIPv4Address())
        {
            hostAddress = QHostAddress(ipAddressesList.at(i).toString());
            break;
        }
    }

    if (hostAddress.isNull())
    {
        hostAddress = QHostAddress(QHostAddress::LocalHost);
    }

    return hostAddress;
}

quint16 Server::getPort() const
{
    return this->serverPort();
}

void Server::addClient()
{
//    qDebug()<<"open : ";
    while(this->hasPendingConnections())
    {
        QTcpSocket *socket =  this->nextPendingConnection();
        clients.insert(socket);
        //qDebug()<<client->isOpen();
        connect(socket, SIGNAL(readyRead()), this, SLOT(readReceivedData()));
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError(QAbstractSocket::SocketError)));
        connect(socket, SIGNAL(disconnected()), this, SLOT(connectionLost()));
        connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
        
        if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.100").toIPv4Address())
        {
            qDebug() << "ACS CLIENT CONNECT"<< socket->socketDescriptor();
            client_agv[0] = socket;
            Q_EMIT client_connect(0, getPort());
        }
        else if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.101").toIPv4Address())
        {
            qDebug()<<"AGV1 CLIENT CONNECT" << socket->socketDescriptor();
            client_agv[1] = socket;
            Q_EMIT client_connect(1, getPort());
        }
        else if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.102").toIPv4Address())
        {
            qDebug()<<"AGV2 CLIENT CONNECT" << socket->socketDescriptor();
            client_agv[2] = socket;
            Q_EMIT client_connect(2, getPort());
        }
    }
}

void Server::disconnected()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    clients.remove(socket);
    
    if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.100").toIPv4Address())
    {
        qDebug()<<"ACS CLIENT DISCONNECT";
        client_agv[0] = nullptr;
        Q_EMIT client_disconnect(0, getPort());
    }
    else if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.101").toIPv4Address())
    {
        qDebug()<<"AGV1 CLIENT DISCONNECT";
        client_agv[1] = nullptr;
        Q_EMIT client_disconnect(1, getPort());
    }

    else if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.102").toIPv4Address())
    {
        qDebug()<<"AGV2 CLIENT DISCONNECT";
        client_agv[2] = nullptr;
        Q_EMIT client_disconnect(2, getPort());
    }

}

void Server::close_()
{
    close();
}

void Server::writeData(QByteArray str)
{
    Q_FOREACH(QTcpSocket *socket, clients)
    {
        if(socket->state() == QAbstractSocket::ConnectedState)
        {
            socket->write(str);

            //return true;
        }
        else
        {
            qDebug()<<"close";
            //return false;
        }
    }
}

void Server::writeData(int id, QByteArray str)
{
    if(client_agv[id]==nullptr)
    {
        qDebug()<<"close";    
        return;
    }

    QTcpSocket *socket = client_agv[id];
    
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        qDebug()<<id<<"write";
        socket->write(str);
        //return true;
    }
    else
    {
        qDebug()<<"close";
        //return false;
    }

}

void Server::readReceivedData()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    QByteArray buffer;
//    qDebug()<< "Server Recieve :" << getPort();
    while(socket->bytesAvailable())
    {
      buffer.append(socket->readLine());
    }
	//0425
    if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.89").toIPv4Address())
    {
        id = 0;
    }
    else if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.101").toIPv4Address())
    {
        id = 1;
    }

    else if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.102").toIPv4Address())
    {
        id = 2;
    }
    else
    {
        return;
    }

	if(getPort()==30003)
	{
	    QByteArray STX, ETX, LEN;
	    STX = buffer.mid(0,1);
	    LEN = buffer.mid(1,2);
	    ETX = buffer.mid(buffer.size()-1,1);
	 
        if( (STX.toHex()=="02") && (ETX.toHex()=="02") && (buffer.size()==QByteToShort(LEN)) )
        {
            emit readInfo(id, buffer.mid(3,23));
	    }
	}
}

void Server::displayError(QAbstractSocket::SocketError error)
{
    qDebug()<<this->getPort()<<error;
}

void Server::connectionLost()
{
    qDebug()<<this->getPort()<<"disconnect";
}

QByteArray IntToArray(qint32 source) //Use qint32 to ensure that the number have 4 bytes
{
    //Avoid use of cast, this is the Qt way to serialize objects
    QByteArray temp;
    QDataStream data(&temp, QIODevice::ReadWrite);
    data << source;
    return temp;
}

