#ifndef TCP_MANAGER_H
#define TCP_MANAGER_H

#include <QtCore>
#include <QObject>

#include <TCP/server.h>
#include <QNode/qnode.hpp>
#include <agv_protocol/convertToByte.hpp>

#include <QTimer>



class tcpManager : public QObject
{
    Q_OBJECT
    
public:
//    explicit 
    tcpManager(int argc, char** argv, QObject *parent = nullptr);
    ~tcpManager();
    
	void setIP(std::string);
	void setPort(quint16);
	
public slots:
    void close();
    void readData(QByteArray);
    void add();

    void send_CMD(quint8 agv_id, quint8 task_cmd, qint8 goal, qint8 sub_goal, qint16 dist_x, qint16 dist_y, qint32 dist_th, qint8 conv_cmd);
    void send_State();
    
    void readInfo(int, QByteArray);
    
signals:
    void finished();
    
private:
    Server *cmd_server;
    Server *state_server;
    
    QNode *server_node;
    
    QTimer *state_Timer;

    STATE state_1, state_2;

    void sendInit(quint8 id, quint8 pos);

};

#endif
