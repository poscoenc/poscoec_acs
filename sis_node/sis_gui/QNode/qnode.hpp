

#ifndef protocol_sample_QNODE_HPP_
#define protocol_sample_QNODE_HPP_

#ifndef Q_MOC_RUN

#include <string>
#include <QThread>
#include <QStringListModel>
#include <Point/point.h>

#endif

#include <ros/ros.h>
#include <ros/network.h>

#include <sis_msgs/state_acs.h>
#include <sis_msgs/state_Server.h>
#include <sis_msgs/state_PLC.h>
#include <sis_msgs/state_Client.h>


#include <sis_msgs/cmd_AGV.h>
#include <sis_msgs/state_AGV.h>
#include <sis_msgs/complete_AGV.h>


#include <std_msgs/Int8.h>

#include "logger/logger.h"
#include <iostream>

#define  AGV_STP  0
#define  GOAL_REACH  3
#define  OBS  11
#define  MANUAL_  12
#define  QR_FAIL  4
#define  LRF_FAIL  5
#define  SEN_FAIL  13
#define  LRF_RECOVER  14
#define  LRF_ER_    15



struct STATE
{
    int8_t id = 0;
    int16_t X = 0;
    int16_t Y = 0;
    int32_t A = 0;

    uint8_t agv_pos = 0;

    uint8_t agv_status = 0;
    uint8_t conv_status = 0;
    uint8_t emc = 0;

    int16_t main_vol = 0;

    int8_t conv_in = 0;
    int8_t conv_mid = 0;
    int8_t conv_arr = 0;

    int8_t stp1_up = 0;
    int8_t stp1_down = 0;
    int8_t stp2_up = 0;
    int8_t stp2_down = 0;
};

struct AXIS
{
    double X = 0.0;
    double Y = 0.0;
    double A = 0.0;
    uint8_t GOAL = 0;
    uint8_t SUB_GOAL = 0;
};

enum AGV_MODE{
    mode_wait_ = 2, mode_stop_=0, mode_run_ = 1, mode_lift_ = 3, mode_emc_ = 4
};


enum AGV_ID{
  NONE_=2, AGV1_= 0, AGV2_=1
};

enum CMD_{
    WAIT       = 6,
    MOVE       = 1,
    STOP       = 0,
    CONV_OPERATE       = 3,
    CONV_STOP     = 2,
};


class QNode : public QThread {
    Q_OBJECT

public:
	QNode(int argc, char** argv );
	virtual ~QNode();
	bool init();
	void run();

    void stateCallback(const sis_msgs::state_AGV::ConstPtr& msg);
    void portCallback(const sis_msgs::state_Server::ConstPtr& msg);
    void cmdCallback(const sis_msgs::cmd_AGV::ConstPtr& msg);

    void send_shutdown();
    AXIS posToXYA1(QString pos_);
    AXIS posToXYA2(QString pos_);

	enum LogLevel {
	         Debug,
	         Info,
	         Warn,
	         Error,
	         Fatal
	 };

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);
    QString dis_pos_[2];

Q_SIGNALS:
	void loggingUpdated();
    void rosShutdown();
    
    //COMMUNICATION to NODE
    void port_state(qint8, bool, bool);
    void agv_state(qint8, STATE);
    void show_WARN(qint8, qint8);
    void ledLim1(int index, bool isOn);
    void ledLim2(int index, bool isOn);
    //void ledAP(int index, bool isOn);

    
    
public Q_SLOTS:
    void CMD_task(quint8 id_, quint8 cmd_, QString pos_, qint8 lift);
    void AGV_command(quint8 id_, quint8 cmd_);


	
private:
	int init_argc;
	char** init_argv;

    QStringListModel logging_model;
    
	ros::Subscriber sub_port;
    ros::Subscriber sub_state;
    ros::Subscriber sub_CMD;
    
    ros::Publisher pub_state;
    ros::Publisher pub_CMD;

    ros::Publisher pub_shutDown;
    STATE state_[2];

    int8_t agvStatus[2], preAgvStatus[2];
    int agvCon_[2], preAgvCon_[2];

    void AGV_task(int id);

    void agvMove(int id, int goal);
    void convOperate(int id, int dir);
    void ledlim1Show();
    void ledlim2Show();

};

#endif /* protocol_sample_QNODE_HPP_ */
