
#include <QNode/qnode.hpp>
#include <ros/network.h>
#include <ros/ros.h>

#include <std_msgs/Empty.h>
#include <std_msgs/Int8.h>
#include <std_msgs/String.h>
#include <tf/tf.h>

#include <sstream>

#include <QDebug>
#include <QSettings>

QNode::QNode(int argc, char** argv) : init_argc(argc), init_argv(argv)
{
  init();
}

QNode::~QNode()
{
  if (ros::isStarted())
  {
    ros::shutdown(); // explicitly needed since we use ros::start();
    ros::waitForShutdown();
  }
  wait();
}

bool QNode::init()
{
  ros::init(init_argc, init_argv, "sis_node");
  if (!ros::master::check())
  {
    return false;
  }
  ros::start(); // explicitly needed since our nodehandle is going out of scope.
  ros::NodeHandle n;

  sub_port = n.subscribe("/Display/AGV/PORT", 1, &QNode::portCallback, this);
  sub_state = n.subscribe("/Display/AGV/STATE", 1, &QNode::stateCallback, this);
  sub_plc = n.subscribe("/PLC/PORT", 1, &QNode::plcCallback, this);

  pub_CMD = n.advertise<sis_msgs::cmd_AGV>("/Display/ACS/CMD", 1, this);

  pub_shutDown = n.advertise<std_msgs::Int8>("/Display/ACS/Shutdown", 1, this);

  pubagvID = n.advertise<sis_msgs::initPos>("/Display/ACS/ID", 1, this);

  start();

  return true;
}

void QNode::run()
{
  ros::Rate loop_rate(5);

  LogWriter::WriteLog();

  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }

  Q_EMIT
  rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)

  ros::shutdown();
}

void QNode::ledlim1Show()
{
  switch (cntlim1)
  {
  case 0:
  {
    if (state_[0].limDown == 1)
    {
      emit ledLim1(0, true);
    }
    else
    {
      emit ledLim1(0, false);
    }
    cntlim1 = 1;
  }
  break;
  case 1:
  {
    if (state_[0].limUp == 1)
    {
      emit ledLim1(1, true);
    }
    else
    {
      emit ledLim1(1, false);
    }
    cntlim1 = 2;
  }
  break;
  case 2:
  {
    if (state_[0].lim1 == 1)
    {
      emit ledLim1(2, true);
    }
    else
    {
      emit ledLim1(2, false);
    }
    cntlim1 = 3;
  }
  break;
  case 3:
  {
    if (state_[0].lim2 == 1)
    {
      emit ledLim1(3, true);
    }
    else
    {
      emit ledLim1(3, false);
    }
    cntlim1 = 4;
  }
  break;
  case 4:
  {
    if (state_[0].lim3 == 1)
    {
      emit ledLim1(4, true);
    }
    else
    {
      emit ledLim1(4, false);
    }
    cntlim1 = 5;
  }
  break;
  case 5:
  {
    if (state_[0].lim4 == 1)
    {
      emit ledLim1(5, true);
    }
    else
    {
      emit ledLim1(5, false);
    }
    cntlim1 = 0;
  }
  break;
  }
}
void QNode::ledlim2Show()
{
  switch (cntlim2)
  {
  case 0:
  {
    if (state_[1].limDown == 1)
    {
      emit ledLim2(0, true);
    }
    else
    {
      emit ledLim2(0, false);
    }
    cntlim2 = 1;
  }
  break;
  case 1:
  {
    if (state_[1].limUp == 1)
    {
      emit ledLim2(1, true);
    }
    else
    {
      emit ledLim2(1, false);
    }
    cntlim2 = 2;
  }
  break;
  case 2:
  {
    if (state_[1].lim1 == 1)
    {
      emit ledLim2(2, true);
    }
    else
    {
      emit ledLim2(2, false);
    }
    cntlim2 = 3;
  }
  break;
  case 3:
  {
    if (state_[1].lim2 == 1)
    {
      emit ledLim2(3, true);
    }
    else
    {
      emit ledLim2(3, false);
    }
    cntlim2 = 4;
  }
  break;
  case 4:
  {
    if (state_[1].lim3 == 1)
    {
      emit ledLim2(4, true);
    }
    else
    {
      emit ledLim2(4, false);
    }
    cntlim2 = 5;
  }
  break;
  case 5:
  {
    if (state_[1].lim4 == 1)
    {
      emit ledLim2(5, true);
    }
    else
    {
      emit ledLim2(5, false);
    }
    cntlim2 = 0;
  }
  break;
  }
}

void QNode::send_shutdown()
{
  std_msgs::Int8 shutdown_msgs;
  shutdown_msgs.data = 1;

  pub_shutDown.publish(shutdown_msgs);
}

void QNode::portCallback(const sis_msgs::state_Server::ConstPtr& msg)
{
  emit port_state(1, msg->AGV1_CMD, msg->AGV1_STATE);

  emit port_state(2, msg->AGV2_CMD, msg->AGV2_STATE);

  if (msg->AGV1_CMD && msg->AGV1_STATE)
  {
    agvCon_[0] = 1;
  }
  else
  {
    agvCon_[0] = 0;
  }

  if (preAgvCon_[0] == 0 && agvCon_[0] == 1)
    qInfo() << "AGV #1 접속중";
  else if (preAgvCon_[0] == 1 && agvCon_[0] == 0)
    qInfo() << "AGV #1 미접속";

  preAgvCon_[0] = agvCon_[0];

  if (msg->AGV2_CMD && msg->AGV2_STATE)
  {
    agvCon_[1] = 1;
  }
  else
  {
    agvCon_[1] = 1;
  }

  if (preAgvCon_[1] == 0 && agvCon_[1] == 1)
    qInfo() << "AGV #2 접속중";
  else if (preAgvCon_[1] == 1 && agvCon_[1] == 0)
    qInfo() << "AGV #2 미접속";

  preAgvCon_[1] = agvCon_[1];
}

void QNode::stateCallback(const sis_msgs::state_AGV::ConstPtr& msg)
{
  int flag = msg->ID;

  if (flag == 1)
  {
    state_[0].id = msg->ID;
    state_[0].X = msg->X;
    state_[0].Y = msg->Y;
    state_[0].A = msg->A;

    state_[0].main_vol = msg->main_vol;

    state_[0].agv_pos = msg->agv_pos;
    state_[0].agv_status = msg->agv_status;

    state_[0].conv_status = msg->conv_status;

    state_[0].emc = msg->emc;

    state_[0].conv_in = msg->conv_in;
    state_[0].conv_mid = msg->conv_mid;
    state_[0].conv_arr = msg->conv_arr;

    state_[0].stp1_up = msg->stp1_up;
    state_[0].stp1_down = msg->stp1_down;
    state_[0].stp2_up = msg->stp2_up;
    state_[0].stp2_down = msg->stp2_down;

    emit agv_state(flag, state_[0]);
  }
  else if (flag == 2)
  {
      state_[1].id = msg->ID;
      state_[1].X = msg->X;
      state_[1].Y = msg->Y;
      state_[1].A = msg->A;

      state_[1].main_vol = msg->main_vol;

      state_[1].agv_pos = msg->agv_pos;
      state_[1].agv_status = msg->agv_status;

      state_[1].conv_status = msg->conv_status;

      state_[1].emc = msg->emc;

      state_[1].conv_in = msg->conv_in;
      state_[1].conv_mid = msg->conv_mid;
      state_[1].conv_arr = msg->conv_arr;

      state_[1].stp1_up = msg->stp1_up;
      state_[1].stp1_down = msg->stp1_down;
      state_[1].stp2_up = msg->stp2_up;
      state_[1].stp2_down = msg->stp2_down;

      emit agv_state(flag, state_[1]);
}

void QNode::AGV_command(quint8 id, quint8 cmd_)
{

}

void QNode::CMD_task(quint8 id_, quint8 cmd_, QString pos_, qint8 conv)
{
  sis_msgs::cmd_AGV task_msgs;

  task_msgs.id = id_;
  task_msgs.cmd = cmd_;

  dis_pos_[id_ - 1] = pos_;

  if (id_ == 1)
  {
    task_msgs.x = posToXYA1(pos_).X;
    task_msgs.y = posToXYA1(pos_).Y;
    task_msgs.th = posToXYA1(pos_).A;
    task_msgs.goal = posToXYA1(pos_).GOAL;
    task_msgs.subgoal = posToXYA1(pos_).SUB_GOAL;
  }
  else if (id_ == 2)
  {
    task_msgs.x = posToXYA2(pos_).X;
    task_msgs.y = posToXYA2(pos_).Y;
    task_msgs.th = posToXYA2(pos_).A;
    task_msgs.goal = posToXYA2(pos_).GOAL;
    task_msgs.subgoal = posToXYA2(pos_).SUB_GOAL;
  }
  else
  {
    task_msgs.x = posToXYA1(pos_).X;
    task_msgs.y = posToXYA1(pos_).Y;
    task_msgs.th = posToXYA1(pos_).A;
    task_msgs.goal = posToXYA1(pos_).GOAL;
    task_msgs.subgoal = posToXYA1(pos_).SUB_GOAL;
  }

  task_msgs.lift = lift;
  if (task_msgs.goal != 0)
    pub_CMD.publish(task_msgs);
}

AXIS QNode::posToXYA1(QString pos_)
{
  AXIS a;

  QSettings settings("ACS", "AGV1");

  if (settings.contains(pos_))
  {
    int8_t id_ = settings.value(pos_).value<QStringList>()[0].toInt();
    if (id_ == MBR1 || id_ == MBR2 || id_ == MBR3)
    {
      a.GOAL = MBR;
      a.SUB_GOAL = id_;
    }
    else if (id_ == WGN1 || id_ == WGN2 || id_ == WGN3)
    {
      a.GOAL = WGN;
      a.SUB_GOAL = id_;
    }
    else if (id_ == PL1 || id_ == PL2)
    {
      a.GOAL = PL;
      a.SUB_GOAL = id_;
    }
    else
    {
      a.GOAL = id_;
      a.SUB_GOAL = 0;
    }

    a.X = settings.value(pos_).value<QStringList>()[1].toDouble();
    a.Y = settings.value(pos_).value<QStringList>()[2].toDouble();
    a.A = settings.value(pos_).value<QStringList>()[3].toDouble();
  }

  return a;
}

AXIS QNode::posToXYA2(QString pos_)
{
  AXIS a;

  QSettings settings("ACS", "AGV2");

  if (settings.contains(pos_))
  {
    uint8_t id_ = settings.value(pos_).value<QStringList>()[0].toInt();
    if (id_ == MBR1 || id_ == MBR2 || id_ == MBR3)
    {
      a.GOAL = MBR;
      a.SUB_GOAL = id_;
    }
    else if (id_ == WGN1 || id_ == WGN2 || id_ == WGN3)
    {
      a.GOAL = WGN;
      a.SUB_GOAL = id_;
    }
    else if (id_ == PL1 || id_ == PL2)
    {
      a.GOAL = PL;
      a.SUB_GOAL = id_;
    }
    else
    {
      a.GOAL = id_;
      a.SUB_GOAL = 0;
    }

    a.X = settings.value(pos_).value<QStringList>()[1].toDouble();
    a.Y = settings.value(pos_).value<QStringList>()[2].toDouble();
    a.A = settings.value(pos_).value<QStringList>()[3].toDouble();
  }
  return a;
}
