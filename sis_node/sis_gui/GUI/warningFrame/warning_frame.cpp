#include "warning_frame.h"
#include "ui_warning_frame.h"
#include <QTableWidgetItem>

#include <QDebug>

warningFrame::warningFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::warningFrame)
{
    ui->setupUi(this);

    init_layout();
    init_table();
    init_config();
}

warningFrame::~warningFrame()
{
    delete ui;
}

void warningFrame::init_layout()
{
    cb_Year = ui->comboBox;
    cb_Month = ui->comboBox_2;
    cb_Date = ui->comboBox_3;
    
    QDate date = QDate::currentDate();

    cb_Year->setItemText( 0, QString::number(date.year()));
    cb_Month->setItemText(0, QString::number(date.month()));
    cb_Date->setItemText( 0, QString::number(date.day()));
}

void warningFrame::init_table()
{
    // init row & col
    ui->table_Log_1->setRowCount(7);
    ui->table_Log_1->setColumnCount(2);
    ui->table_Log_1->horizontalHeader()->resizeSection(0, 130);
    ui->table_Log_1->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
    ui->table_Log_1->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->table_Log_1->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->table_Log_2->setRowCount(7);
    ui->table_Log_2->setColumnCount(2);
    ui->table_Log_2->horizontalHeader()->resizeSection(0, 130);
    ui->table_Log_2->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
    ui->table_Log_2->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->table_Log_2->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    // init header & config
    QStringList state_header = QStringList() << tr("시  간")  << tr("내  용");

    ui->table_Log_1->setHorizontalHeaderLabels(state_header);
    ui->table_Log_1->horizontalHeader()->setFont(font);
    ui->table_Log_1->horizontalHeader()->setFixedHeight(35);
    ui->table_Log_1->verticalHeader()->setFont(font);
    ui->table_Log_1->verticalHeader()->setFixedWidth(30);
    ui->table_Log_1->setSelectionMode(QAbstractItemView::NoSelection);
    ui->table_Log_1->setFocusPolicy(Qt::NoFocus);

    ui->table_Log_2->setHorizontalHeaderLabels(state_header);
    ui->table_Log_2->horizontalHeader()->setFont(font);
    ui->table_Log_2->horizontalHeader()->setFixedHeight(35);
    ui->table_Log_2->verticalHeader()->setFont(font);
    ui->table_Log_2->verticalHeader()->setFixedWidth(30);
    ui->table_Log_2->setSelectionMode(QAbstractItemView::NoSelection);
    ui->table_Log_2->setFocusPolicy(Qt::NoFocus);

/*
    QTableWidgetItem *item_state = new QTableWidgetItem("13:37:00");
    item_state->setTextAlignment(Qt::AlignCenter);
    ui->table_Log_2->setItem(0,0,item_state);
*/
}

void warningFrame::init_config()
{
    font.setPointSize(15);
    for(int i = 0;i++;i<2)
    {
        ct[i] = 0;
    }
}

void warningFrame::show_WARN(qint8 id, qint8 alm)
{
    switch(id)
    {
        case 1:
        {
            QString msg;
            switch(alm)
            {
                case 1:
                {
                    msg = QString("미접속");
                }
                break;
                case 2:
                {
                    msg = QString("QR에러");
                }
                break;

                case 3:
                {
                    msg = QString("장애물");
                }
                break;
                case 4:
                {
                    msg = QString("PLC 비상신호");
                }
                break;
                case 5:
                {
                    msg = QString("기존 AGV 간섭");
                }
                break;
                default: msg = QString(""); break;
            }

            QTableWidgetItem *item2 = new QTableWidgetItem(msg);
            item2->setFont(font);
            item2->setTextAlignment(Qt::AlignCenter|Qt::AlignVCenter);

            QTime cur_time = QTime::currentTime();
            QString time_ = cur_time.toString("h:m:s ap");
            QTableWidgetItem *item1 = new QTableWidgetItem(time_);
            item1->setFont(font);
            item1->setTextAlignment(Qt::AlignCenter|Qt::AlignVCenter);

            if(ct[0] > 6)
                ct[0] = 1;
            else
                ct[0] ++;

            ui->table_Log_1->setItem(ct[0]-1, 0, item1);
            ui->table_Log_1->setItem(ct[0]-1, 1, item2);
/*
            ui->table_Log_1->setItem(0, 0, item1);
            ui->table_Log_1->setItem(0, 1, item2);
*/

        }
        break;

        case 2:
        {
            QString msg;
            switch(alm)
            {
                case 1:
                {
                     msg = QString("EMC ALARM");
                }
                break;
                case 2:
                {
                    msg = QString("OUT OF MAGNETIC RANGE");
                }
                break;

                case 3:
                {
                    msg = QString("PACKET STUCK AT CONVEYOR");
                }
                break;
                default: msg = QString(""); break;

            }
            QTableWidgetItem *item2 = new QTableWidgetItem(msg);
            item2->setFont(font);
            item2->setTextAlignment(Qt::AlignCenter|Qt::AlignVCenter);

            QTime cur_time = QTime::currentTime();
            QString time_ = cur_time.toString("h:m:s ap");
            QTableWidgetItem *item1 = new QTableWidgetItem(time_);
            item1->setFont(font);
            item1->setTextAlignment(Qt::AlignCenter|Qt::AlignVCenter);

            if(ct[1] > 6)
                ct[1] = 1;
            else
                ct[1] ++;

            ui->table_Log_2->setItem(ct[1]-1, 0, item1);
            ui->table_Log_2->setItem(ct[1]-1, 1, item2);

        }
        break;

    }
}
