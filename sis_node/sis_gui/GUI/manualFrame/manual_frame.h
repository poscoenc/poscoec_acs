#ifndef MANUAL_FRAME_H
#define MANUAL_FRAME_H

#include <QWidget>
#include <QPushButton>
#include <GUI/config/btn_style.h>
#include <QLineEdit>


namespace Ui {
class manualFrame;
}

class QLabel;
class QTableWidgetItem;

class cmd_frame;

class manualFrame : public QWidget
{
    Q_OBJECT

public:
    explicit manualFrame(QWidget *parent = 0);
    ~manualFrame();


signals:
    void cmd_task(qint8 , QString, QString);


private slots:

    void btn_CMD_pressed(int i);
    void btn_CMD_clicked(int i);
    void btn_CMD_released(int i);

    void posMtoUpper(QString &text);
    void posTtoUpper(QString &text);



private:
    Ui::manualFrame *ui;
    void manualF_init_layout();
    void manualF_init_config();
    void set_BTN_Selected(int id, bool isSelect);


    QPushButton *btn_CMD[5];
    QLineEdit *lineMPos,*lineTPos;



    enum CMD_{
        WAIT       = 6,
        M_RUN       = 1,
        M_STP       = 0,
        T_STP       = 2,
        T_LOADING     = 3,
        T_UNLOADING   = 4
    };
    enum TASK_CMD{
        LOADING = 1,
        UNLOADING = 2,
        STOP = 3
    };

    enum BTN{
        MRUN = 0,
        MSTP = 1,
        LOAD = 2,
        UNLOAD = 3,
        TASKSTP = 4
    };

};

#endif // MANUAL_FRAME_H
