#include "manual_frame.h"
#include "ui_manual_frame.h"

manualFrame::manualFrame(QWidget* parent)
    : QWidget(parent), ui(new Ui::manualFrame)
{
  ui->setupUi(this);
  manualF_init_layout();
  manualF_init_config();
}

manualFrame::~manualFrame() { delete ui; }

void manualFrame::manualF_init_layout()
{
  btn_CMD[BTN::MRUN] = ui->btnMRun;
  btn_CMD[BTN::MSTP] = ui->btnMStp;
  btn_CMD[BTN::LOAD] = ui->btnLoading;
  btn_CMD[BTN::UNLOAD] = ui->btnUnloading;
  btn_CMD[BTN::TASKSTP] = ui->btnTaskStp;
  lineMPos = ui->line_mPos;
  lineTPos = ui->line_tPos;
}

void manualFrame::manualF_init_config()
{
  for (int i = 0; i < 5; i++)
  {
    connect(btn_CMD[i], &QPushButton::clicked, [=]() { btn_CMD_clicked(i); });
    connect(btn_CMD[i], &QPushButton::pressed, [=]() { btn_CMD_pressed(i); });
    connect(btn_CMD[i], &QPushButton::released, [=]() { btn_CMD_released(i); });
    set_BTN_Selected(i, false);
  }
  connect(lineMPos, SIGNAL(textChanged(QString&)), this,
          SLOT(posMtoUpper(QString&)));
  connect(lineTPos, SIGNAL(textChanged(QString&)), this,
          SLOT(posTtoUpper(QString&)));
}

void manualFrame::set_BTN_Selected(int id, bool isSelect)
{

  if (id < 0 || id > 4)
    return;

  if (isSelect)
  {
    btn_CMD[id]->setStyleSheet(style_Select);
  }
  else
  {

    btn_CMD[id]->setStyleSheet(style_default);
  }
}

void manualFrame::posMtoUpper(QString& text)
{
  lineMPos = qobject_cast<QLineEdit*>(sender());
  if (!lineMPos)
    return;

  QString uc_text(text.toUpper());
  if (uc_text != text)
  {
    bool ps = lineMPos->blockSignals(true);
    lineMPos->setText(uc_text);
    lineMPos->blockSignals(ps);
  }
}

void manualFrame::posTtoUpper(QString& text)
{
  lineTPos = qobject_cast<QLineEdit*>(sender());
  if (!lineTPos)
    return;

  QString uc_text(text.toUpper());
  if (uc_text != text)
  {
    bool ps = lineTPos->blockSignals(true);
    lineTPos->setText(uc_text);
    lineTPos->blockSignals(ps);
  }
}

void manualFrame::btn_CMD_clicked(int id)
{
  switch (id)
  {
  case BTN::MRUN:
  {
    set_BTN_Selected(BTN::MRUN, true);
    set_BTN_Selected(BTN::MSTP, false);
    QString posM_ = lineMPos->text().toUpper();
    QString PosM(posM_);
    lineMPos->setText(PosM);
    QString posT_ = "";
    emit cmd_task(CMD_::M_RUN, posM_, posT_);
  }
  break;
  case BTN::MSTP:
  {
    set_BTN_Selected(BTN::MRUN, false);
    set_BTN_Selected(BTN::MSTP, true);
    QString posM_ = "";
    QString posT_ = "";
    emit cmd_task(CMD_::M_STP, posM_, posT_);
  }
  break;
  case BTN::TASKSTP:
  {
    set_BTN_Selected(BTN::TASKSTP, true);
    set_BTN_Selected(BTN::LOAD, false);
    set_BTN_Selected(BTN::UNLOAD, false);
    QString posM_ = "";
    QString posT_ = "";
    emit cmd_task(CMD_::T_STP, posM_, posT_);
  }
  break;
  case BTN::LOAD:
  {
    set_BTN_Selected(BTN::TASKSTP, false);
    set_BTN_Selected(BTN::LOAD, true);
    set_BTN_Selected(BTN::UNLOAD, false);
    QString posT_ = lineTPos->text().toUpper();
    QString PosT(posT_);
    lineTPos->setText(PosT);
    QString posM_ = "";
    emit cmd_task(CMD_::T_LOADING, posM_, posT_);
  }
  break;
  case BTN::UNLOAD:
  {
    set_BTN_Selected(BTN::TASKSTP, false);
    set_BTN_Selected(BTN::LOAD, false);
    set_BTN_Selected(BTN::UNLOAD, true);
    QString posT_ = lineTPos->text().toUpper();
    QString PosT(posT_);
    lineTPos->setText(PosT);
    QString posM_ = "";
    emit cmd_task(CMD_::T_UNLOADING, posM_, posT_);
  }
  break;
  }
}

void manualFrame::btn_CMD_pressed(int id) { set_BTN_Selected(id, true); }

void manualFrame::btn_CMD_released(int id) { set_BTN_Selected(id, true); }
