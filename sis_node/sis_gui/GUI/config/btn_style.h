#ifndef BTN_STYLE_H
#define BTN_STYLE_H

#include <QString>

const QString style_Select = QString(
                                "QPushButton:pressed"
                                "{"
                                "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))"
                                "}"
                                "QPushButton"
                                "{"
                                "     background-color: #3cbaa2;"
                                "     border: 2px solid black;"
                                "     border-radius: 5px;"
                                "     color: #f9f9f9;"
                                "     font: 80 20pt;"
                                "     font-weight: 550;"
                                "}"
                                "QPushButton:disabled"
                                "{"
                                "    background-color: "//rgb(170, 170, 127)"
                                "}"
                            );
const QString style_Warning = QString(
                                "QPushButton:pressed"
                                "{"
                                "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))"
                                "}"
                                "QPushButton"
                                "{"
                                "     background-color: #ba3f56;"
                                "     border: 2px solid black;"
                                "     border-radius: 5px;"
                                "     color: #f9f9f9;"
                                "     font: 80 20pt;"
                                "     font-weight: 550;"
                                "}"
                                "QPushButton:disabled"
                                "{"
                                "    background-color: "//rgb(170, 170, 127)"
                                "}"
                            );
const QString style_default = QString(
                                "QPushButton"
                                "{"
                                "     font: 75 15pt;"
                                "     font-weight: 400;"
                                "}"
                                "QPushButton:pressed"
                                "{"
                                "     background-color: #3cbaa2;"
                                "     border: 2px solid black;"
                                "     font: 90 15pt;"
                                "     border-radius: 5px;"
                                "     font-weight: 550;"
                                "}"
                                "QPushButton:disabled"
                                "{"
                                "    background-color: "//rgb(170, 170, 127)"
                                "}"
                          );

const QString style_nor = QString(
                                "QPushButton"
                                "{"
                                "     font: 75 12pt;"
                                "     font-weight: 400;"
                                "}"
                                "QPushButton:pressed"
                                "{"
                                "     background-color: #3cbaa2;"
                                "     border: 2px solid black;"
                                "     font: 90 12pt;"
                                "     border-radius: 5px;"
                                "     font-weight: 550;"
                                "}"
                                "QPushButton:disabled"
                                "{"
                                "    background-color: "//rgb(170, 170, 127)"
                                "}"
                          );
const QString style_sel = QString(
                                "QPushButton:pressed"
                                "{"
                                "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))"
                                "}"
                                "QPushButton"
                                "{"
                                "     background-color: #3cbaa2;"
                                "     border: 2px solid black;"
                                "     border-radius: 5px;"
                                "     color: #f9f9f9;"
                                "     font: 80 12pt;"
                                "     font-weight: 550;"
                                "}"
                                "QPushButton:disabled"
                                "{"
                                "    background-color: "//rgb(170, 170, 127)"
                                "}"
                            );
//#3cbaa2

//#dfecf7
//#4080c2
//#1f425d
#endif // AGV_COMMAND_H
