#include "maingui.h"
#include "ui_maingui.h"

#include <QProcess>

MainGUI::MainGUI(QWidget* parent)
    : QMainWindow(parent),
      setframe_ui(new set_frame(this)),
      connect_ui(new connect_frame(this)),
      warning_ui(new warningFrame(this)),
      viz_ui(new myViz(this)),
      size_icon(80, 80),
      qnode(new QNode(0, 0)),
      ui(new Ui::MainGUI)
{
  ui->setupUi(this);

  init_Layout();
  init_Config();
  init_Menu();

  btn_menu[Menu::Connect]->click();

  timer = new QTimer(this);
  QObject::connect(timer, SIGNAL(timeout()), this, SLOT(sub_loop()));

  // ros & loop start: looplate 500
  ros::start();
  timer->start(500);
}

MainGUI::~MainGUI()
{

  delete setframe_ui;
  delete warning_ui;
  delete viz_ui;
  delete connect_ui;

  delete ui;
}

void MainGUI::sub_loop()
{
  if (ros::ok())
  {
    QPixmap pix;
    pix.load(":/resource/LOGO.png");

    pix = pix.scaled(ui->label_logo->size(), Qt::KeepAspectRatio);
    ui->label_logo->setAlignment(Qt::AlignVCenter);
    ui->label_logo->setPixmap(pix);
  }
  else
  {
    ros::shutdown();
    ros::waitForShutdown();
    QApplication::quit();
  }
}

void MainGUI::init_Config()
{
  // set Font
  font.setBold(false);
  font.setPointSize(22);

  text = QStringList() << tr("  파라미터") << tr("  종  료") << tr("  모니터링")
                       << tr("  에러로그");
  path = QStringList() << tr("option.png") << tr("exit_2.png")
                       << tr("display.png") << tr("warning.png");
  path_w = QStringList() << tr("option_w.png") << tr("exit_2_w.png")
                         << tr("display_w.png") << tr("warning_w.png");

  // STATE
  QObject::connect(qnode, SIGNAL(agvPos(qint16, qint16, qint32)), viz_ui,
                   SLOT(agvPos(qint16, qint16, qint32)));

  QObject::connect(qnode,
                   SIGNAL(agvStatus(qint8, qint8, qint16, qint16, qint32, qint8,
                                    qint16, qint16, qint16)),
                   connect_ui,
                   SLOT(agvStatus(qint8, qint8, qint16, qint16, qint32, qint8,
                                  qint16, qint16, qint16)));


  QObject::connect(qnode, SIGNAL(connectState(int, bool)), connect_ui,
                   SLOT(connectState(int, bool)));
  QObject::connect(qnode, SIGNAL(ioPort(qint8)), connect_ui,
                   SLOT(ioPort(qint8)));
  QObject::connect(qnode, SIGNAL(updateProgress(qint16)), connect_ui,
                   SLOT(updateProgress(qint16)));

  // CMD
  QObject::connect(connect_ui, SIGNAL(CMD_task(qint8, QString, QString)),
                   qnode, SLOT(CMD_task(qint8, QString, QString)));

}

void MainGUI::init_Layout()
{
  ui->display_Layout->addWidget(viz_ui);

  ui->command_Layout->addWidget(setframe_ui);
  ui->command_Layout->addWidget(connect_ui);
  ui->command_Layout->addWidget(warning_ui);

  ui->menu_Layout->setAlignment(Qt::AlignRight);

  warning_ui->close();
  setframe_ui->close();
  connect_ui->close();

  // set Icon

  btn_menu[Menu::Option] = ui->btn_Option;
  btn_menu[Menu::Exit] = ui->btn_Exit;
  btn_menu[Menu::Connect] = ui->btn_Connect;
  btn_menu[Menu::Warning] = ui->btn_Warning;

  for (int i = 0; i < 4; i++)
  {
    btn_menu[i]->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    btn_menu[i]->setMaximumSize(250, 96);
    btn_menu[i]->setMinimumSize(250, 95);

    connect(btn_menu[i], &QPushButton::clicked, [=]() { btn_clicked(i); });
    connect(btn_menu[i], &QPushButton::pressed, [=]() { btn_pressed(i); });
    connect(btn_menu[i], &QPushButton::released, [=]() { btn_released(i); });
  }
}

void MainGUI::init_Menu()
{
  for (int i = 0; i < 4; i++)
  {
    setSelected(i, false);
  }
}

void MainGUI::setSelected(int id, bool isSelect)
{
  // Modify
  if (id < 0 || id > 3)
    return;

  if (isSelect)
  {
    pixmap[id] = QPixmap(":/resource/" + path[id]);
    btn_menu[id]->setStyleSheet("QPushButton"
                                "{"
                                "    border: 2px inset #a3a1a1;"
                                "    background-color: blue;"
                                "    background-color: #3cbaa2;"
                                "    background-color: rgb(239, 239, 239);"
                                "    color:rgb(40, 40, 40);"
                                "    font-weight: 550;"
                                "}");
  }
  else
  {
    pixmap[id] = QPixmap(":/resource/" + path_w[id]);

    btn_menu[id]->setStyleSheet("QPushButton"
                                "{"
                                "    background-color: rgb(225, 225, 201);"
                                "    background-color: #1f425d;"
                                "    border: 2px outset #a3a1a1;"
                                "    font: 75 20pt;"
                                "    font-weight: 550;"
                                "    color:rgb(240, 240, 240);"
                                "}"

                                "QPushButton:pressed"
                                "{"
                                "    border: 2px inset #a3a1a1;"
                                "    background-color: blue;"
                                "    background-color: #3cbaa2;"
                                "    background-color: rgb(239, 239, 239);"
                                "    color:rgb(40, 40, 40);"
                                "    font-weight: 550;"
                                "}");
  }
  icon[id] = QIcon(pixmap[id]);

  btn_menu[id]->setIcon(icon[id]);
  btn_menu[id]->setIconSize(size_icon);
  btn_menu[id]->setFont(font);
  btn_menu[id]->setText(text[id]);
}

void MainGUI::btn_pressed(int id)
{
  //    qDebug()<<"pressed"<<i;
  setSelected(id, true);
}

void MainGUI::btn_clicked(int id)
{
  init_Menu();
  setSelected(id, true);

  switch (id)
  {

  case Menu::Option: // TODO Parameter_UI
    connect_ui->close();
    warning_ui->close();
    setframe_ui->show();
    setframe_ui->set_posTable(9);
    setframe_ui->set_taskTable(6);
    break;

  case Menu::Exit: // TODO Exit & ShutDown
    connect_ui->close();
    warning_ui->close();
    setframe_ui->close();
    qnode->send_shutdown();
    ros::shutdown();
    close();
    break;

  case Menu::Connect: // TODO Network_UI
    setframe_ui->close();
    warning_ui->close();
    connect_ui->show();
    break;

  case Menu::Warning: // TODO Error Log_UI
    connect_ui->close();
    setframe_ui->close();
    warning_ui->close();
    break;
  default:
    break;
  }
}

void MainGUI::btn_released(int id)
{
  init_Menu();
  setSelected(id, true);
}
