#ifndef MAINGUI_H
#define MAINGUI_H


#include <ros/ros.h>
#include <QMainWindow>
#include <QFormLayout>

#include <GUI/warningFrame/warning_frame.h>
#include <GUI/connectFrame/connect_frame.h>
#include <GUI/setupFrame/set_frame.h>

#include <Vizualization/myviz.h>

#include <QNode/qnode.hpp>

#include <QTimer>
#include <QDebug>
#include <QFileInfo>



namespace Ui {
class MainGUI;
}

class warningFrame;
class connect_frame;
class set_frame;
class myViz;

class MainGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainGUI(QWidget *parent = 0);
    ~MainGUI();

    void init_Config();
    void init_Layout();
    void init_Menu();
    // menu
    void setSelected(int id, bool);


public slots:
    // loop-Function
    void sub_loop();


private slots:
    void btn_pressed(int id);
    void btn_clicked(int id);
    void btn_released(int id);

private:
    // loop-object
    ros::NodeHandle nh;
    QTimer *timer;

    Ui::MainGUI *ui;
    QPushButton *btn_menu[4];

    QFont font;
    QPixmap pixmap[4];
    QIcon icon[4];
    QStringList text, path, path_w;

    set_frame       *setframe_ui;
    warningFrame    *warning_ui;
    connect_frame *connect_ui;
    // Map-Object
    myViz           *viz_ui;

    // Menu-Icon size
    const QSize size_icon;


    QNode *qnode;

    enum Menu
    {
        Option = 0,
        Exit    = 1,
        Connect = 2,
        Warning = 3,
    };
};

#endif // MAINGUI_H
