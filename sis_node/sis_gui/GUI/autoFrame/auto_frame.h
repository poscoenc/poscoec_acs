#ifndef autoFrame_H
#define autoFrame_H

#include <QWidget>
#include <QDebug>
#include <GUI/config/btn_style.h>
#include <QPushButton>


namespace Ui {
class autoFrame;
}

class QLabel;
class QTableWidgetItem;


class autoFrame : public QWidget
{
    Q_OBJECT

public:
    explicit autoFrame(QWidget *parent = 0);
    ~autoFrame();

signals:
    void auto_command(qint8 );

public slots:
    void btn_STA_pressed();
    void btn_STA_clicked();
    void btn_STA_released();

    void btn_STP_pressed();
    void btn_STP_clicked();
    void btn_STP_released();

private:
    Ui::autoFrame *ui;

    void autoF_init_layout();
    void autoF_init_config();
    QPushButton *btn_STA;
    void set_btn_STA_selected(bool selected);
    QPushButton *btn_STP;
    void set_btn_STP_selected(bool selected);


};

#endif // autoFrame_H
