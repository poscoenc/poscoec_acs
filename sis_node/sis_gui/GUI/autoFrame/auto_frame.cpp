#include "auto_frame.h"
#include "ui_auto_frame.h"

autoFrame::autoFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::autoFrame)
{
    ui->setupUi(this);
    autoF_init_layout();
    autoF_init_config();
}

autoFrame::~autoFrame()
{
    delete ui;
}


void autoFrame::autoF_init_config()
{
    set_btn_STP_selected(false);
    set_btn_STA_selected(false);
}

void autoFrame::autoF_init_layout()
{

    btn_STA = ui->btnSta;
    btn_STP= ui->btnStp;

    connect(btn_STA, &QPushButton::clicked, [=]() { btn_STA_clicked(); });
    connect(btn_STA, &QPushButton::pressed, [=]() { btn_STA_pressed(); });
    connect(btn_STA, &QPushButton::released, [=]() { btn_STA_released(); });

    connect(btn_STP, &QPushButton::clicked, [=]() { btn_STP_clicked(); });
    connect(btn_STP, &QPushButton::pressed, [=]() { btn_STP_pressed(); });
    connect(btn_STP, &QPushButton::released, [=]() { btn_STP_released(); });

}

void autoFrame::set_btn_STA_selected(bool selected)
{
  if (selected)
  {
    btn_STA->setStyleSheet(style_Select);
  }
  else
  {
    btn_STA->setStyleSheet(style_default);
  }
}
void autoFrame::set_btn_STP_selected(bool selected)
{
  if (selected)
  {
    btn_STP->setStyleSheet(style_Select);
  }
  else
  {
    btn_STP->setStyleSheet(style_default);
  }
}

void autoFrame::btn_STA_clicked()
{
  // task_init_config();
  set_btn_STP_selected(true);

  emit auto_command(1);
  set_btn_STP_selected(false);
}

void autoFrame::btn_STA_pressed()
{
  set_btn_STA_selected(true);
}
void autoFrame::btn_STA_released()
{
  set_btn_STA_selected(true);
}

void autoFrame::btn_STP_clicked()
{
  // task_init_config();
  set_btn_STP_selected(true);

  emit auto_command(0);
  set_btn_STA_selected(false);
}

void autoFrame::btn_STP_pressed()
{
  set_btn_STP_selected(true);
}
void autoFrame::btn_STP_released()
{
  set_btn_STP_selected(true);
}
