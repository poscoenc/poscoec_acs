#ifndef CONNECT_FRAME_H
#define CONNECT_FRAME_H

#include <GUI/object/kled.h>
#include <QPushButton>
#include <QWidget>

#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QTableWidget>
#include <QProgressBar>

#include <GUI/config/btn_style.h>
#include <string>

#define AGV1 0
#define AGV2 1
#define L2 2

namespace Ui
{
class connect_frame;
}

class QLabel;
class KLed;


class connect_frame : public QWidget
{
  Q_OBJECT

public:
  explicit connect_frame(QWidget* parent = 0);
  ~connect_frame();

  void closeEvent(QCloseEvent* event);

  void conn_init_layout();
  void conn_init_config();



  enum PORT{
      CMD_ = 1,
      STATE_ = 2
  };



  enum AGV_STATUS
  {
    AGV_STP = 0,
    GOAL_REACH = 3,
    LRF_RUN = 1,
    OBS = 4,
    MANUAL = 2,
  };

  enum CONV_STATUS
  {
    CONV_STP = 0,
    CONV_MAN = 1,
    CONV_MOV = 2,
    CONV_GOAL = 3
  };

public slots:
  void agvStatus(qint8, qint8, qint16, qint16, qint32, qint8, qint16, qint16, qint16);
  void connectState(int, bool);
  void updateProgress(qint8, qint16);
  void ioPort(qint8);


private slots:

  void btn_mode_clicked(int i);
  void btn_mode_pressed(int i);
  void btn_mode_released(int i);

  void btn_loadmode_click(int i);


signals:
  void CMD_task(qint8, QString, QString);

private:
  Ui::connect_frame* ui;

  QFont font, ft;
  KLed* ledPort_[3];
  QGridLayout* layoutPort[3];
  QGridLayout* layoutBat[2];
  QGridLayout* layoutConv1[3];
  QGridLayout* layoutConv2[3];
  QGridLayout* layoutEmc[2];
  QGridLayout* layoutStp1[4];
  QGridLayout* layoutStp2[4];

  QLineEdit* pos1[3];
  QLineEdit* pos2[3];
  QLabel* lab1[2];
  QLabel* lab2[2];

  bool isConnect[3];
  void ledPort(int index, bool isOn);

  QPushButton* btnAuto1[2];
  QPushButton* btnAuto2[2];
  QPushButton* btnMan1[5];
  QPushButton* btnMan2[5];

  QLineEdit* posId1;
  QLineEdit* posId2;

  void set_btn_Selected(int id, bool isSelect);

  QProgressBar* Vol1, Vol2;
  int16_t vol_min = 46;
  int16_t vol_max = 62;
  QString task_pos = "";

};

#endif // CONNECT_FRAME_H
