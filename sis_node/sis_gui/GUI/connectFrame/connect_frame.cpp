﻿#include "connect_frame.h"
#include "ui_connect_frame.h"

#include <QDebug>

connect_frame::connect_frame(QWidget* parent)
    : QWidget(parent), ui(new Ui::connect_frame)
{
  ui->setupUi(this);
  font.setPointSize(14);
  ft.setBold(false);
  ft.setPointSize(10);
  conn_init_layout();
  conn_init_config();
}

connect_frame::~connect_frame()
{
  for (int i = 0; i < 3; i++)
  {
    delete ledPort_[i];
  }
  delete ui;
}

void connect_frame::closeEvent(QCloseEvent* event)
{
  QWidget::closeEvent(event);
}
void connect_frame::conn_init_layout()
{
  // INIT PORT

  for (int i = 0; i < 3; i++)
  {
    ledPort_[i] = new KLed(this);
    ledPort_[i]->setShape(KLed::Shape::Rectangular);
    ledPort_[i]->setColor(Qt::red);
  }

  rackNo = ui->rackNo;
  layoutBat = ui->gridBat;
}

void connect_frame::updateProgress(qint8 id, qint16 voltage)
{
    if(id == 1)
    {
        int vol_ = (int)(voltage / 10);
        if (vol_ > vol_max)
            vol_ = vol_max;
        else if (vol_ < vol_min)
            vol_ = vol_min;
        Vol1->setValue(vol_);
        QPalette p = Vol1->palette();


        if (vol_ <= 49)
            p.setColor(QPalette::Highlight, QColor(Qt::red));
        else if (vol_ < 52)
            p.setColor(QPalette::Highlight, QColor(Qt::yellow));
        else if (vol_ < 56)
            p.setColor(QPalette::Highlight, QColor(Qt::green));
        else
            p.setColor(QPalette::Highlight, QColor(Qt::red));

        Vol1->setPalette(p);
        int remain = vol_ % 10;
        Vol1->setFormat(QString("%1.%2(V)").arg(vol_).arg(remain));
    }
    if(id == 2)
    {
        int vol_ = (int)(voltage / 10);
        if (vol_ > vol_max)
            vol_ = vol_max;
        else if (vol_ < vol_min)
            vol_ = vol_min;
        Vol2->setValue(vol_);
        QPalette p = Vol2->palette();


        if (vol_ <= 49)
            p.setColor(QPalette::Highlight, QColor(Qt::red));
        else if (vol_ < 52)
            p.setColor(QPalette::Highlight, QColor(Qt::yellow));
        else if (vol_ < 56)
            p.setColor(QPalette::Highlight, QColor(Qt::green));
        else
            p.setColor(QPalette::Highlight, QColor(Qt::red));

        Vol2->setPalette(p);
        int remain = vol_ % 10;
        Vol2->setFormat(QString("%1.%2(V)").arg(vol_).arg(remain));
    }
}

void connect_frame::set_btn_Selected(int id, bool isSelect)
{

  if (id < 0 || id > 13)
    return;

  if (isSelect)
  {
    btnMode[id]->setStyleSheet(style_sel);
  }
  else
  {

    btnMode[id]->setStyleSheet(style_nor);
  }
}

void connect_frame::btn_loadmode_click(int i)
{
  switch (i)
  {
  case AUTO_LOAD:
  {
    btnLoadMode[AUTO_LOAD]->setStyleSheet(style_sel);
    btnLoadMode[MANUAL_LOAD]->setStyleSheet(style_nor);
    _loadMode = 0;
  }
  break;
  case MANUAL_LOAD:
  {
    btnLoadMode[AUTO_LOAD]->setStyleSheet(style_nor);
    btnLoadMode[MANUAL_LOAD]->setStyleSheet(style_sel);
    _loadMode = 1;
  }
  break;
  }
}

void connect_frame::btn_mode_clicked(int i)
{
  switch (i)
  {
  case BTN_STP:
  {
    for (int j = 0; j < 14; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(i, true);
    qint8 cmd = CMD_MSTP;
    QString m_goal = "";
    QString task_goal = "";

    emit CMD_task(cmd, m_goal, task_goal);

    qint8 cmd2 = CMD_TSTP;
    QString m_goal2 = "";
    QString task_goal2 = "";
    emit CMD_task(cmd2, m_goal2, task_goal2);
    _btn_clicked = -1;
  }
  break;
  case BTN_M_CHG:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);
    qint8 cmd = CMD_MOVE;
    QString m_goal = "충전";
    // QString m_goal = "CHG";
    QString task_goal = "";

    emit CMD_task(cmd, m_goal, task_goal);
    _btn_clicked = i;
  }
  break;
  case BTN_M_SHOP:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_MOVE;
    QString m_goal = "자재";
    // QString m_goal = "SHOP";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_M_TEST:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);
    qint8 cmd = CMD_MOVE;
    QString m_goal = "시험";
    // QString m_goal = "TEST";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_M_R11:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_MOVE;
    QString m_goal = "환경01-L";
    // QString m_goal = "RA1L";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_M_R12:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_MOVE;
    QString m_goal = "환경01-R";
    // QString m_goal = "RA1R";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_M_R21:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_MOVE;
    QString m_goal = "환경02-L";
    // QString m_goal = "RA2L";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_M_R22:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_MOVE;
    QString m_goal = "환경01-R";
    // QString m_goal = "RA2R";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_M_R31:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_MOVE;
    QString m_goal = "환경03-L";
    // QString m_goal = "RA3L";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_M_R32:
  {
    for (int j = 0; j < 9; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_MOVE;
    QString m_goal = "환경03-R";
    // QString m_goal = "RA3R";
    QString task_goal = "";
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_LOAD_H:
  {
    for (int j = 9; j < 13; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_LOAD;
    QString m_goal = "환경01-L";
    // QString m_goal = "RA1R";
    QString task_goal = "";
    _rackNo = rackNo->text().toInt();
    if (_loadMode == 0)
      task_goal = QString(task_pos) + "-H";
    else
    {
      switch (_rackNo)
      {
      case 1:
      {
        task_goal = QString("TQ01-H");
      }
      break;
      case 2:
      {
        task_goal = QString("TQ02-H");
      }
      break;
      case 3:
      {
        task_goal = QString("TQ03-H");
      }
      break;
      }
    }
    qDebug() << "task:" << task_goal;
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_UNLOAD_H:
  {
    for (int j = 9; j < 13; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_UNLOAD;
    QString m_goal = "환경01-L";
    // QString m_goal = "RA1R";
    QString task_goal = "";
    _rackNo = rackNo->text().toInt();
    if(_loadMode == 0)
     task_goal = QString(task_pos) + "-H";
    else
    {
      switch (_rackNo)
      {
      case 1:
      {
        task_goal = QString("TQ01-H");
      }
      break;
      case 2:
      {
        task_goal = QString("TQ02-H");
      }
      break;
      case 3:
      {
        task_goal = QString("TQ03-H");
      }
      break;
      }
    }
    qDebug() << "task:" << task_goal;
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_LOAD_L:
  {
    for (int j = 9; j < 13; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_LOAD;
    QString m_goal = "환경01-L";
    // QString m_goal = "RA1R";
    QString task_goal = "";
    _rackNo = rackNo->text().toInt();
    if(_loadMode == 0)
     task_goal = QString(task_pos) + "-L";
    else
    {
      switch (_rackNo)
      {
      case 1:
      {
        task_goal = QString("TQ01-L");
      }
      break;
      case 2:
      {
        task_goal = QString("TQ02-L");
      }
      break;
      case 3:
      {
        task_goal = QString("TQ03-L");
      }
      break;
      }
    }
    qDebug() << "task:" << task_goal;
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  case BTN_UNLOAD_L:
  {
    for (int j = 9; j < 13; j++)
    {
      set_btn_Selected(j, false);
    }
    set_btn_Selected(BTN_STP, false);
    set_btn_Selected(i, true);

    qint8 cmd = CMD_UNLOAD;
    QString m_goal = "환경01-L";
    // QString m_goal = "RA1R";
    QString task_goal = "";
    _rackNo = rackNo->text().toInt();
    if(_loadMode == 0)
     task_goal = QString(task_pos) + "-L";
    else
    {
      switch (_rackNo)
      {
      case 1:
      {
        task_goal = QString("TQ01-L");
      }
      break;
      case 2:
      {
        task_goal = QString("TQ02-L");
      }
      break;
      case 3:
      {
        task_goal = QString("TQ03-L");
      }
      break;
      }
    }
    qDebug() << "task:" << task_goal;
    _btn_clicked = i;

    emit CMD_task(cmd, m_goal, task_goal);
  }
  break;
  }
}

void connect_frame::ioPort(qint8 connect)
{
   if(connect == 1)
        ledPort(0, true);
   else
       ledPort(0, false);
}

void connect_frame::btn_mode_pressed(int i) { set_btn_Selected(i, true); }
void connect_frame::btn_mode_released(int i) { set_btn_Selected(i, true); }

void connect_frame::conn_init_config()
{
  for (int i = 0; i < 14; i++)
  {

    connect(btnMode[i], &QPushButton::clicked, [=]() { btn_mode_clicked(i); });
    connect(btnMode[i], &QPushButton::pressed, [=]() { btn_mode_pressed(i); });
    connect(btnMode[i], &QPushButton::released,
            [=]() { btn_mode_released(i); });
  }
  for (int i=0;i<2;i++)
  {
      connect(btnLoadMode[i], &QPushButton::clicked, [=]() { btn_loadmode_click(i); });
  }
  progressBar = new QProgressBar();
  progressBar->setMinimum(46);
  progressBar->setMaximum(62);

  progressBar->setVisible(true);
  progressBar->setTextVisible(true);
  progressBar->setFormat(QString("Voltage (V)"));
  progressBar->setAlignment(Qt::AlignCenter);
  int vol_ = 0;
  progressBar->setValue(vol_);
  progressBar->setFormat(QString("%1 (V)").arg(vol_));
  progressBar->setFixedHeight(58);

  layoutBat->addWidget(progressBar);
}

void connect_frame::connectState(int id, bool state) { ledPort(id, state); }

void connect_frame::ledPort(int index, bool isOn)
{
  if (isOn)
    ledPort_[index]->setColor(Qt::green);
  else if (!isOn)
    ledPort_[index]->setColor(Qt::red);
}

void connect_frame::agvStatus(qint8 mov, qint8 pos, qint16 px, qint16 py,
                              qint32 pa, qint8 task, qint16 z, qint16 table,
                              qint16 pusher)
{
  ui->line_posX->setText(QString::number(px / 1000.0, 'f', 2));
  ui->line_posY->setText(QString::number(py / 1000.0, 'f', 2));
  ui->line_posA->setText(QString::number(pa / 1000.0, 'f', 2));
  ui->line_Z->setText(QString::number(z / 1000.0, 'f', 2));
  ui->line_Table->setText(QString::number(table / 1000.0, 'f', 2));
  ui->line_Pusher->setText(QString::number(pusher / 1000.0, 'f', 2));
  switch (mov)
  {
  case AGV_STATUS::AGV_STP:
    ui->label_Mstatus->setText("STOP");
    break;
  case AGV_STATUS::LRF_RUN:
    ui->label_Mstatus->setText("RUN");
    break;
  case AGV_STATUS::MANUAL:
    ui->label_Mstatus->setText("MANUAL");
    break;
  case AGV_STATUS::LRF_FAIL:
    ui->label_Mstatus->setText("ERROR");
    break;
  case AGV_STATUS::OBS:
    ui->label_Mstatus->setText("OBS");
    break;
  case AGV_STATUS::WP_REACH:
    ui->label_Mstatus->setText("RUN");
    break;
  case AGV_STATUS::LRF_XY_COMP:
    ui->label_Mstatus->setText("RUN");
    break;
  case AGV_STATUS::GOAL_REACH:
    ui->label_Mstatus->setText("GOAL");
    break;
  case AGV_STATUS::QR_RUN:
    ui->label_Mstatus->setText("QR RUN");
    break;
  }

  if(pre_moveStatus == AGV_STATUS::QR_RUN && mov == AGV_STATUS::GOAL_REACH)
    {
        if(_btn_clicked<9)
        {
          set_btn_Selected(_btn_clicked, false);
        }
    }
    pre_moveStatus = mov;

  switch (task)
  {
  case TASK_STATUS::TASK_STP:
    ui->label_Fstatus->setText("STOP");
    break;
  case TASK_STATUS::TASK_MAN:
    ui->label_Fstatus->setText("MANUAL");
    break;
  case TASK_STATUS::TASK_GOAL:
    ui->label_Fstatus->setText("GOAL");
    break;
  case TASK_STATUS::TASK_MOV:
    ui->label_Fstatus->setText("RUN");
    break;
  }

  if(pre_taskStatus == TASK_STATUS::TASK_MOV && task == TASK_STATUS::TASK_GOAL)
  {
      if(_btn_clicked>8)
      {
        set_btn_Selected(_btn_clicked, false);
      }
  }
  pre_taskStatus = task;

  switch (pos)
  {
  case LOADING_POS:
    ui->label_Pos->setText("자재");
    break;
  case FIRE_POS:
    ui->label_Pos->setText("시험");
    break;
  case CHG:
    ui->label_Pos->setText("충전");
    break;
  case RA1L:
    ui->label_Pos->setText("환경01-L");
    task_pos = "TQ01";
    break;
  case RA1R:
    ui->label_Pos->setText("환경01-R");
    task_pos = "TQ01";
    break;
  case RA2L:
    ui->label_Pos->setText("환경02-L");
    task_pos = "TQ02";
    break;
  case RA2R:
    ui->label_Pos->setText("환경01-R");
    task_pos = "TQ02";
    break;
  case RA3L:
    ui->label_Pos->setText("환경03-L");
    task_pos = "TQ03";
    break;
  case RA3R:
    ui->label_Pos->setText("환경01-R");
    task_pos = "TQ03";
    break;
  case NO_WHERE:
    ui->label_Pos->setText("NONE");
    break;
  }
}
