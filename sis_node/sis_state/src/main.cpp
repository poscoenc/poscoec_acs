#include <QProcess>
#include <ros/ros.h>
#include <sis_msgs/state_AGV.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>
#include <visualization_msgs/Marker.h>

using namespace tf;

visualization_msgs::Marker marker_1;
visualization_msgs::Marker marker_2;

ros::Publisher pub_vis_1;
ros::Publisher pub_vis_2;

uint32_t shape = visualization_msgs::Marker::ARROW;

void posCallback(const sis_msgs::state_AGV::ConstPtr& msg)
{
  if (msg->ID == 1)
  {
    marker_1.header.frame_id = "map"; // = world
    marker_1.header.stamp = ros::Time::now();
    marker_1.ns = "basic_shapes";
    marker_1.id = 1;

    marker_1.type = shape;

    marker_1.action = visualization_msgs::Marker::ADD;

    marker_1.pose.position.x = msg->X / 1000.0;
    marker_1.pose.position.y = msg->Y / 1000.0;
    marker_1.pose.position.z = 0;
    marker_1.pose.orientation = tf::createQuaternionMsgFromYaw(
        static_cast<double>((msg->A / 1000.0) * M_PI / 180.0));

    marker_1.color.r = 0.0f;
    marker_1.color.g = 1.0f;
    marker_1.color.b = 0.0f;
    marker_1.color.a = 1.0;
    // %Tag(SCALE)%
    marker_1.scale.x = 2.0;
    marker_1.scale.y = 2.0;
    marker_1.scale.z = 2.0;
    // %EndTag(SCALE)%
    marker_1.lifetime = ros::Duration();

    pub_vis_1.publish(marker_1);
  }

  if (msg->ID == 2)
  {

    marker_2.header.frame_id = "map"; // = world
    marker_2.header.stamp = ros::Time::now();
    marker_2.ns = "basic_shapes";
    marker_2.id = 2;

    marker_2.type = shape;

    marker_2.action = visualization_msgs::Marker::ADD;

    marker_2.pose.position.x = msg->X / 1000.0;
    marker_2.pose.position.y = msg->Y / 1000.0;
    marker_2.pose.position.z = 0;
    marker_2.pose.orientation = tf::createQuaternionMsgFromYaw(
        static_cast<double>((msg->A / 1000.0) * M_PI / 180.0));

    marker_2.color.r = 0.0f;
    marker_2.color.g = 0.0f;
    marker_2.color.b = 1.0f;
    marker_2.color.a = 1.0;
    marker_2.lifetime = ros::Duration();
    // %Tag(SCALE)%
    marker_2.scale.x = 2.0;
    marker_2.scale.y = 2.0;
    marker_2.scale.z = 2.0;
    // %EndTag(SCALE)%

    pub_vis_2.publish(marker_2);
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "sis_state_node");
  ros::NodeHandle nh_;

  pub_vis_1 = nh_.advertise<visualization_msgs::Marker>(
      "/Display/visualization_marker_1", 0);

  pub_vis_2 = nh_.advertise<visualization_msgs::Marker>(
      "/Display/visualization_marker_2", 0);

  ros::Subscriber sub_pos = nh_.subscribe("/Display/AGV/STATE", 1, posCallback);
  ///////////////////////////////////////////////////////////////////
  static tf::TransformBroadcaster br;

  tf::Transform transform;
  transform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));

  tf::Quaternion q;
  q.setRPY(0, 0, 0);

  transform.setRotation(q);
  ///////////////////////////////////////////////////////////////////
  ros::Rate loop_rate(1);

  int64_t count_ = 0;

  while (ros::ok())
  {
    br.sendTransform(
        tf::StampedTransform(transform, ros::Time::now(), "world", "map"));
    ros::spinOnce();
    count_++;
    if (count_ == 3600)
    {
      count_ = 0;
      QProcess process;
      QStringList list;
      list << "/home/sis/sh/clean.sh";
      process.execute("/bin/bash", list);
    }
    loop_rate.sleep();
  }
  return 0;
}
